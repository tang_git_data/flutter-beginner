import 'dart:ui';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MoonStep2Controller extends GetxController {
  final double seaLevel = Get.height - 180.0;
  final double finalPosition = 300;
  late double _mooenCenterY;
  get mooenCenterY => _mooenCenterY;
  late double _waveY;
  get waveY => _waveY;
  double _waveRadius = 200.0;
  get waveRadius => _waveRadius;

  int _waveMoveCount = 0;
  final int waveMoveStep = 20;
  bool moveForward = true;
  bool first = true;
  late Timer _downcountTimer;

  @override
  void onInit() {
    _mooenCenterY = seaLevel;
    _waveY = 0;
    super.onInit();
  }

  @override
  void onReady() {
    _downcountTimer = Timer.periodic(Duration(milliseconds: 40), repaint);
    super.onReady();
  }

  void repaint(Timer timer) {
    bool needUpdate = false;
    if (_mooenCenterY > finalPosition) {
      _mooenCenterY -= 1;
      needUpdate = true;
    } else {
      timer.cancel();
    }

    _waveMoveCount++;
    int maxStep = first ? waveMoveStep : waveMoveStep * 2;
    if (moveForward) {
      _waveY += 0.5;
    } else {
      _waveY -= 0.5;
    }
    if (_waveMoveCount > maxStep) {
      _waveMoveCount = 0;
      first = false;
      moveForward = !moveForward;
    }

    if (needUpdate) {
      update();
    }
  }

  @override
  void onClose() {
    _downcountTimer.cancel();
    super.onClose();
  }
}

class DrawSeaPage extends StatelessWidget {
  DrawSeaPage({Key? key}) : super(key: key);
  final MoonStep2Controller controller = MoonStep2Controller();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MoonStep2Controller>(
      init: controller,
      builder: (store) => CustomPaint(
        child: null,
        foregroundPainter: MoonStep1Painter(
          mooenCenterY: store.mooenCenterY,
          seaLevel: store.seaLevel,
          waveY: store.waveY,
          waveRadius: store.waveRadius,
        ),
      ),
    );
  }
}

class MoonStep1Painter extends CustomPainter {
  final double mooenCenterY;
  final double seaLevel;
  final double waveY;
  final double waveRadius;
  MoonStep1Painter({
    required this.mooenCenterY,
    required this.seaLevel,
    required this.waveY,
    required this.waveRadius,
  }) : super();

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawColor(Color(0xFF252525), BlendMode.color);
    var center = size / 2;
    paintMooen(canvas, Offset(center.width, mooenCenterY), 90);
    paintSea(canvas, size);
    paintPoet(canvas, '海上升明月 天涯共此时', size);
  }

  void paintMooen(Canvas canvas, Offset center, double raidus) {
    var mooenPaint = Paint()..color = Colors.yellow[100]!;
    mooenPaint.strokeWidth = 2.0;
    canvas.drawCircle(
      center,
      raidus,
      mooenPaint,
    );

    var lightPaint = Paint()..color = Colors.yellow[100]!.withAlpha(30);
    lightPaint.strokeWidth = 2.0;
    canvas.drawCircle(
      center,
      raidus + 3,
      lightPaint,
    );
  }

  void paintSea(Canvas canvas, Size size) {
    var seaPaint = Paint()..color = Color(0xFF020408);
    seaPaint.strokeWidth = 2.0;
    Path backPath = Path();
    backPath.moveTo(0, seaLevel);
    int backCount = 6;
    for (var i = 0; i < backCount + 1; ++i) {
      if (i % 2 == 0) {
        backPath.arcToPoint(
            Offset(size.width / backCount + i * size.width / backCount,
                seaLevel + waveY),
            radius: Radius.circular(waveRadius));
      } else {
        backPath.arcToPoint(
            Offset(size.width / backCount + i * size.width / backCount,
                seaLevel + waveY),
            radius: Radius.circular(waveRadius),
            clockwise: false);
      }
    }
    backPath.lineTo(size.width, size.height);
    backPath.lineTo(0, size.height);
    canvas.drawPath(backPath, seaPaint);

    Path middlePath = Path();
    middlePath.moveTo(size.width, seaLevel);
    int middleWaveCount = 4;
    for (var i = 1; i < middleWaveCount + 1; ++i) {
      if (i % 2 == 0) {
        middlePath.arcToPoint(
            Offset(size.width - i * size.width / middleWaveCount,
                seaLevel - waveY),
            radius: Radius.circular(waveRadius),
            clockwise: false);
      } else {
        middlePath.arcToPoint(
            Offset(size.width - i * size.width / middleWaveCount,
                seaLevel - waveY),
            radius: Radius.circular(waveRadius),
            clockwise: true);
      }
    }
    middlePath.lineTo(0, size.height);
    middlePath.lineTo(size.width, size.height);

    canvas.drawPath(middlePath, seaPaint);

    Path frontPath = Path();
    frontPath.moveTo(0, seaLevel);
    int frondCount = 8;
    for (var i = 0; i < frondCount + 1; ++i) {
      if (i % 2 == 0) {
        frontPath.arcToPoint(
            Offset(size.width / frondCount + i * size.width / frondCount,
                seaLevel + waveY + 10),
            radius: Radius.circular(waveRadius),
            clockwise: false);
      } else {
        frontPath.arcToPoint(
            Offset(size.width / frondCount + i * size.width / frondCount,
                seaLevel + waveY + 10),
            radius: Radius.circular(waveRadius),
            clockwise: true);
      }
    }
    frontPath.lineTo(size.width, size.height);
    frontPath.lineTo(0, size.height);
    canvas.drawPath(frontPath, seaPaint);
  }

  void paintPoet(Canvas canvas, String poet, Size size) {
    var style = TextStyle(
      fontWeight: FontWeight.w300,
      fontSize: 26.0,
      color: Colors.yellow[100],
    );

    final ParagraphBuilder paragraphBuilder = ParagraphBuilder(
      ParagraphStyle(
        fontSize: style.fontSize,
        fontFamily: style.fontFamily,
        fontStyle: style.fontStyle,
        fontWeight: style.fontWeight,
        textAlign: TextAlign.center,
      ),
    )
      ..pushStyle(style.getTextStyle())
      ..addText(poet);
    final Paragraph paragraph = paragraphBuilder.build()
      ..layout(ParagraphConstraints(width: size.width));
    canvas.drawParagraph(paragraph, Offset(0, 100));
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
