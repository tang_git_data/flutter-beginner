class Person {
  final String name;
  final String gender;

  const Person({required this.name, required this.gender});
}
