import 'package:flutter/material.dart';
import 'package:home_framework/states/face_emotion.dart';
import 'package:home_framework/states/model_binding_v2.dart';

class Leisi extends StatelessWidget {
  Leisi({Key? key}) : super(key: key) {
    print('ModelBinding=====constructor: 雷思');
  }

  @override
  Widget build(BuildContext context) {
    print('ModelBinding=====build：雷思');
    return Center(
      child:
          Text('雷思感受到了小芙的${ModelBindingV2.of<FaceEmotion>(context).emotion}'),
    );
  }
}
