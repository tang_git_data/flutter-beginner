import 'package:flutter/material.dart';
import '../../view_models/dynamic/dynamic_share_model.dart';
import '../../view_models/dynamic/dynamic_model.dart';
import 'package:provider/provider.dart';
import 'dynamic_form.dart';

class _DynamicAdd extends StatefulWidget {
  _DynamicAdd({Key? key}) : super(key: key);

  _DynamicAddState createState() => _DynamicAddState();
}

class _DynamicAddState extends State<_DynamicAdd> {
  @override
  void initState() {
    super.initState();
    context.read<DynamicShareModel>().clearState();
  }

  @override
  Widget build(BuildContext context) {
    final watchState = context.watch<DynamicShareModel>();
    final readState = context.read<DynamicShareModel>();
    return Scaffold(
      appBar: AppBar(
        title: Text('添加动态'),
        brightness: Brightness.dark,
      ),
      body: DynamicForm(
        watchState.formData,
        readState.handleTextFieldChanged,
        readState.handleClear,
        '提交',
        () {
          readState.handleAdd().then((success) {
            if (success) {
              context.read<DynamicModel>().add(readState.currentDynamic!);
              Navigator.of(context).pop();
            }
          });
        },
        readState.handleImagePicked,
        imageFile: watchState.imageFile,
      ),
    );
  }
}

class DynamicAddWrapper extends StatelessWidget {
  DynamicAddWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: DynamicShareModel.sharedDynamicModel,
      child: _DynamicAdd(),
    );
  }
}
