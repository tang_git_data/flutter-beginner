class MessageEntity {
  late String _fromUserId;
  String get fromUserId => _fromUserId;

  late String _toUserId;
  String get toUserId => _toUserId;

  late String _contentType;
  String get contentType => _contentType;

  late String _content;
  String get content => _content;

  static MessageEntity fromJson(Map<String, dynamic> json) {
    MessageEntity message = new MessageEntity();
    message._fromUserId = json['fromUserId']!;
    message._toUserId = json['toUserId']!;
    message._contentType = json['contentType']!;
    message._content = json['content']!;

    return message;
  }

  Map<String, String> toJson() {
    return {
      'fromUserId': _fromUserId,
      'toUserId': _toUserId,
      'content': _content,
      'contentType': _contentType,
    };
  }
}
