import 'package:flutter/cupertino.dart';
import 'package:home_framework/models/message_entity.dart';

class ChatMessageModel with ChangeNotifier {
  List<MessageEntity> _messages = [];
  List<MessageEntity> get messages => _messages;

  String content = '';

  void addMessage(Map<String, dynamic> json) {
    _messages.add(MessageEntity.fromJson(json));
  }
}
