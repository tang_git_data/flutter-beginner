import 'package:flutter/material.dart';

extension ThemeColor on Color {
  static const primaryColor = Color(0xFF0990EC);
  static const lineColor = Color(0xFFEEEEEE);

  Color get reverseColor {
    return Color.fromARGB(
      alpha,
      255 - red,
      255 - green,
      255 - blue,
    );
  }

  String toHex() {
    // 将颜色转换为16进制字符串表示，例如：#FFFF0000
    return '#${value.toRadixString(16).padLeft(6, 'F').toUpperCase()}';
  }
}
