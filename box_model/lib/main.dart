import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter 盒子模型',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('盒子模型'),
        ),
        body: Center(
          child: Container(
            width: 300,
            height: 300,
            color: Colors.blue,
            child: Container(
              color: Colors.red,
              margin: EdgeInsets.fromLTRB(10, 0, 20, 30),
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                color: Colors.white60,
                child: Text('这是一长段文字，这是一长段文字,这是一长段文字，这是一长段文字,这是一长段文字，这是一长段文字'),
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
