import 'package:flutter/material.dart';

class SizeTransitionDemo extends StatefulWidget {
  SizeTransitionDemo({Key? key}) : super(key: key);

  @override
  _SizeTransitionDemoState createState() => _SizeTransitionDemoState();
}

class _SizeTransitionDemoState extends State<SizeTransitionDemo>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller =
      AnimationController(duration: const Duration(seconds: 3), vsync: this)
        ..repeat();

  //使用自定义曲线动画过渡效果
  late Animation<double> _animation =
      CurvedAnimation(parent: _controller, curve: Curves.linear);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SizeTransition'),
        brightness: Brightness.dark,
        backgroundColor: Colors.blue,
      ),
      body: paintShow(),
    );
  }

  @override
  void dispose() {
    _controller.stop();
    _controller.dispose();
    super.dispose();
  }

  Widget superman() {
    return SizeTransition(
      child: Center(
        child: Image.asset(
          'images/superman.png',
          width: 300.0,
          height: 300.0,
        ),
      ),
      sizeFactor: _animation,
      axis: Axis.horizontal,
      axisAlignment: 1.0,
    );
  }

  Widget paintShow() {
    return Container(
      alignment: Alignment.center,
      child: SizeTransition(
        child: Image.asset(
          'images/juanzhou.png',
        ),
        sizeFactor: _animation,
        axis: Axis.horizontal,
        axisAlignment: 0.0,
      ),
    );
  }
}
