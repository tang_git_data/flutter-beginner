import 'package:flutter/material.dart';

import 'round_image.dart';

class HeroDetail extends StatelessWidget {
  final String tag;
  final String assetImageName;
  const HeroDetail({Key? key, required this.tag, required this.assetImageName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero 基础动画详情'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: SizedBox(
          width: 200,
          height: 200,
          child: Hero(
            tag: this.tag,
            child: RoundImage(
              onTap: () {
                Navigator.of(context).pop();
              },
              assetImageName: this.assetImageName,
              imageSize: 200.0,
            ),
          ),
        ),
      ),
    );
  }
}
