import 'package:flutter/material.dart';

class ColorBox extends StatelessWidget {
  final double width;
  final double height;
  final Color color;
  final Widget? child;
  const ColorBox({
    Key? key,
    required this.width,
    required this.height,
    required this.color,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Color(0xFFA0A0A0),
            blurRadius: 6.0,
            offset: Offset(2, 4),
          ),
        ],
      ),
      child: child,
    );
  }
}
