import 'package:flutter/material.dart';

class AnimatedSwitcherDemo extends StatefulWidget {
  AnimatedSwitcherDemo({Key? key}) : super(key: key);

  @override
  _AnimatedSwitcherDemoState createState() => _AnimatedSwitcherDemoState();
}

class _AnimatedSwitcherDemoState extends State<AnimatedSwitcherDemo> {
  Widget? _animatedWidget;
  bool test = false;

  @override
  void initState() {
    super.initState();
    _animatedWidget = ClipOval(
      child: Image.asset('images/beauty.jpeg'),
      key: ValueKey(1),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedSwitcher'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: AnimatedSwitcher(
            child: _animatedWidget,
            duration: const Duration(milliseconds: 1000),
            transitionBuilder: (child, animation) {
              return SizeTransition(
                sizeFactor: animation,
                child: child,
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: () {
          setState(() {
            test = !test;
            _animatedWidget = test
                ? ClipOval(
                    child: Image.asset('images/beauty2.jpeg'),
                    key: ValueKey(2),
                  )
                : ClipOval(
                    child: Image.asset('images/beauty.jpeg'),
                    key: ValueKey(1),
                  );
          });
        },
      ),
    );
  }
}
