import 'dart:math';

import 'package:flutter/material.dart';

class SineCurve extends Curve {
  final int count;
  const SineCurve({this.count = 1}) : assert(count > 0);

  @override
  double transformInternal(double t) {
    // 需要补偿pi/2个角度，使得起始值是0.终止值是1，避免出现最后突然回到0
    return sin(2 * (count + 0.25) * pi * t);
  }
}
