class PartialRefreshState {
  final int praiseCount;
  final int favorCount;

  PartialRefreshState({required this.favorCount, required this.praiseCount});
}
