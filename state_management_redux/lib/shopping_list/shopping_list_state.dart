import 'shopping_item.dart';

class ShoppingListState {
  final List<ShoppingItem> shoppingItems;

  ShoppingListState({required this.shoppingItems});

  factory ShoppingListState.initial() => ShoppingListState(shoppingItems: []);

  bool operator ==(Object? other) {
    if (other == null || !(other is ShoppingListState)) return false;

    if (other.shoppingItems.length != shoppingItems.length) return false;

    for (int i = 0; i < other.shoppingItems.length; i++) {
      if (other.shoppingItems[i] != shoppingItems[i]) return false;
    }
    return true;
  }

  @override
  get hashCode => shoppingItems.hashCode;
}
