import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:state_management_redux/counter/counter_actions.dart';
import 'package:state_management_redux/counter/counter_state.dart';

import 'counter_reducer.dart';

class CounterPage extends StatelessWidget {
  CounterPage({Key? key}) : super(key: key);
  final store =
      Store<CounterState>(counterReducer, initialState: CounterState(0));
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Redux Counter'),
        ),
        body: Center(
          child: StoreConnector<CounterState, String>(
            converter: (store) => store.state.count.toString(),
            builder: (context, count) {
              return Text(
                count,
                style: TextStyle(color: Colors.blue, fontSize: 30),
              );
            },
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FloatingActionButton(
                child: Icon(Icons.arrow_upward),
                onPressed: () {
                  store.dispatch(CounterAddAction());
                }),
            FloatingActionButton(
                backgroundColor: Colors.red,
                child: Icon(
                  Icons.arrow_downward,
                ),
                onPressed: () {
                  store.dispatch(CounterSubAction());
                }),
          ],
        ),
      ),
    );
  }
}
