import 'package:flutter/material.dart';

class AboutDialogDemo extends StatefulWidget {
  const AboutDialogDemo({Key? key}) : super(key: key);

  @override
  State<AboutDialogDemo> createState() => _AboutDialogDemoState();
}

class _AboutDialogDemoState extends State<AboutDialogDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('About Dialog Demo'),
        actions: [
          IconButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                applicationName: '岛上码农',
                applicationVersion: '1.0.0',
                applicationIcon: Image.asset('images/logo.png'),
                applicationLegalese: '2023 岛上码农版权所有'
              );
            },
            icon: const Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
