import 'package:flutter/material.dart';

class FittedBoxDemo extends StatefulWidget {
  const FittedBoxDemo({Key? key}) : super(key: key);

  @override
  State<FittedBoxDemo> createState() => _FittedBoxDemoState();
}

class _FittedBoxDemoState extends State<FittedBoxDemo> {
  var _fit = BoxFit.none;
  var _alignment = Alignment.center;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
          title: const Text('FittedBox'), backgroundColor: Colors.red[400]!),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width - 30.0,
          height: 160.0,
          color: Colors.blue,
          child: FittedBox(
            alignment: _alignment,
            fit: _fit,
            clipBehavior: Clip.antiAlias,
            child: Image.asset('images/girl.jpeg'),
          ),
        ),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          DropdownButton(
            items: const [
              DropdownMenuItem<BoxFit>(
                value: BoxFit.none,
                child: Text('BoxFit.none'),
              ),
              DropdownMenuItem<BoxFit>(
                value: BoxFit.contain,
                child: Text('BoxFit.contain'),
              ),
              DropdownMenuItem<BoxFit>(
                value: BoxFit.fill,
                child: Text('BoxFit.fill'),
              ),
              DropdownMenuItem<BoxFit>(
                value: BoxFit.cover,
                child: Text('BoxFit.cover'),
              ),
              DropdownMenuItem<BoxFit>(
                value: BoxFit.fitHeight,
                child: Text('BoxFit.fitHeight'),
              ),
              DropdownMenuItem<BoxFit>(
                value: BoxFit.fitWidth,
                child: Text('BoxFit.fitWidth'),
              ),
              DropdownMenuItem<BoxFit>(
                value: BoxFit.scaleDown,
                child: Text('BoxFit.scaleDown'),
              ),
            ],
            value: _fit,
            onChanged: (fit) {
              setState(() {
                _fit = fit as BoxFit;
              });
            },
          ),
          DropdownButton(
            items: const [
              DropdownMenuItem<Alignment>(
                value: Alignment.center,
                child: Text('Alignment.center'),
              ),
              DropdownMenuItem<Alignment>(
                value: Alignment.centerLeft,
                child: Text('Alignment.centerLeft'),
              ),
              DropdownMenuItem<Alignment>(
                value: Alignment.centerRight,
                child: Text('Alignment.centerRight'),
              ),
              DropdownMenuItem<Alignment>(
                value: Alignment.topCenter,
                child: Text('Alignment.topCenter'),
              ),
              DropdownMenuItem<Alignment>(
                value: Alignment.bottomCenter,
                child: Text('Alignment.bottomCenter'),
              ),
              DropdownMenuItem<Alignment>(
                value: Alignment.topLeft,
                child: Text('Alignment.topLeft'),
              ),
            ],
            value: _alignment,
            alignment: AlignmentDirectional.center,
            onChanged: (alignment) {
              setState(() {
                _alignment = alignment as Alignment;
              });
            },
          ),
        ],
      ),
    );
  }
}

class FittedBoxDemo1 extends StatefulWidget {
  const FittedBoxDemo1({Key? key}) : super(key: key);

  @override
  State<FittedBoxDemo1> createState() => _FittedBoxDemo1State();
}

class _FittedBoxDemo1State extends State<FittedBoxDemo1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
          title: const Text('FittedBox'), backgroundColor: Colors.red[400]!),
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 240.0,
              height: 400.0,
              color: Colors.red,
              child: FittedBox(
                alignment: Alignment.topCenter,
                fit: BoxFit.fitWidth,
                child: Container(
                  height: 50.0,
                  width: 160.0,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(160.0),
                        bottomRight: Radius.circular(160.0)),
                    color: Colors.red[800],
                  ),
                ),
              ),
            ),
            ClipOval(
              child: Container(
                width: 80,
                height: 80,
                alignment: Alignment.center,
                color: Colors.yellow[700],
                child: const Text(
                  '開',
                  style: TextStyle(
                    fontSize: 42.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
