import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_stepper/easy_stepper.dart';
import 'package:flutter/rendering.dart';

class EasyStepperDemo extends StatefulWidget {
  const EasyStepperDemo({Key? key}) : super(key: key);

  @override
  State<EasyStepperDemo> createState() => _EasyStepperDemoState();
}

class _EasyStepperDemoState extends State<EasyStepperDemo> {
  var activeStep = 0;
  final finishedColor = Colors.blue[600];
  final toBeFinishedColor = Colors.grey[300];
  final activeColor = Colors.orange;

  int reachedStep = 0;
  int upperBound = 5;
  int activeStep2 = 0;
  Set<int> reachedSteps = <int>{0, 2, 4, 5};
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Stepper Demo'),
      ),
      body: SafeArea(
        //margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
        //height: 80,
        child: SingleChildScrollView(
          // child: Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     Expanded(flex: 1, child: _previousStep(StepEnabling.individual)),
          //     Expanded(
          //       flex: 15,
          //       child: EasyStepper(
          //         activeStep: activeStep2,
          //         reachedSteps: reachedSteps,
          //         lineLength: 100,
          //         lineSpace: 4,
          //         lineType: LineType.dotted,
          //         activeStepBorderColor: Colors.blue,
          //         activeStepIconColor: Colors.blue,
          //         activeStepTextColor: Colors.blue,
          //         activeLineColor: Colors.blueGrey.withOpacity(0.5),
          //         activeStepBackgroundColor: Colors.white,
          //         unreachedStepBackgroundColor:
          //             Colors.blueGrey.withOpacity(0.5),
          //         unreachedStepBorderColor: Colors.blueGrey.withOpacity(0.5),
          //         unreachedStepIconColor: Colors.blueGrey,
          //         unreachedStepTextColor: Colors.blueGrey.withOpacity(0.5),
          //         unreachedLineColor: Colors.blueGrey.withOpacity(0.5),
          //         finishedStepBackgroundColor: Colors.pink.withOpacity(0.5),
          //         finishedStepBorderColor: Colors.blueGrey.withOpacity(0.5),
          //         finishedStepIconColor: Colors.blueGrey,
          //         finishedStepTextColor: Colors.pink.withOpacity(0.5),
          //         finishedLineColor: Colors.pink.withOpacity(0.5),
          //         borderThickness: 2,
          //         internalPadding: 15,
          //         showStepBorder: true,
          //         showLoadingAnimation: false,
          //         stepRadius: 20,
          //         showTitle: true,
          //         steps: [
          //           EasyStep(
          //             icon: const Icon(CupertinoIcons.cart),
          //             title: '购物车',
          //             lineText: '添加收货地址',
          //             enabled: _allowTabStepping(0, StepEnabling.individual),
          //           ),
          //           EasyStep(
          //             icon: const Icon(CupertinoIcons.info),
          //             title: '填写地址',
          //             lineText: '付款结算',
          //             enabled: _allowTabStepping(1, StepEnabling.individual),
          //           ),
          //           EasyStep(
          //             icon: const Icon(CupertinoIcons.cart_fill_badge_plus),
          //             title: '结算',
          //             lineText: '选择付款方式',
          //             enabled: _allowTabStepping(2, StepEnabling.individual),
          //           ),
          //           EasyStep(
          //             icon: const Icon(CupertinoIcons.money_dollar),
          //             title: '付款',
          //             lineText: '核对订单',
          //             enabled: _allowTabStepping(3, StepEnabling.individual),
          //           ),
          //           EasyStep(
          //             icon: const Icon(Icons.file_present_rounded),
          //             title: '确认订单',
          //             lineText: '提交订单',
          //             enabled: _allowTabStepping(4, StepEnabling.individual),
          //           ),
          //           EasyStep(
          //             icon: const Icon(Icons.check_circle_outline),
          //             title: '完成',
          //             enabled: _allowTabStepping(5, StepEnabling.individual),
          //           ),
          //         ],
          //         onStepReached: (index) => setState(() {
          //           activeStep2 = index;
          //         }),
          //       ),
          //     ),
          //     Expanded(flex: 1, child: _nextStep(StepEnabling.individual)),
          //   ],
          // ),
          child: EasyStepper(
            direction: Axis.horizontal,
            disableScroll: false,
            internalPadding: 0,
            padding: const EdgeInsetsDirectional.symmetric(
                horizontal: 10, vertical: 40),
            alignment: Alignment.center,
            activeStep: activeStep,
            lineLength: 50,
            lineSpace: 10,
            lineType: LineType.normal,
            finishedLineColor: finishedColor,
            activeStepTextColor: activeColor,
            finishedStepTextColor: finishedColor,
            defaultLineColor: toBeFinishedColor,
            //unreachedLineColor: Colors.red,
            showLoadingAnimation: true,
            loadingAnimation: 'images/animation.json',
            stepRadius: 8.0,
            // steppingEnabled: false,
            // enableStepTapping: false,
            showStepBorder: false,
            steps: [
              EasyStep(
                customStep: CircleAvatar(
                  radius: activeStep == 0 ? 12.0 : 8.0,
                  backgroundColor: _getStepColor(0),
                ),
                title: '待付款',
              ),
              EasyStep(
                customStep: CircleAvatar(
                  radius: activeStep == 1 ? 12.0 : 8.0,
                  backgroundColor: _getStepColor(1),
                ),
                title: '已付款',
                topTitle: true,
              ),
              EasyStep(
                customStep: CircleAvatar(
                  radius: activeStep == 2 ? 12.0 : 8.0,
                  backgroundColor: _getStepColor(2),
                ),
                title: '运输中',
              ),
              EasyStep(
                customStep: CircleAvatar(
                  radius: activeStep == 3 ? 12.0 : 8.0,
                  backgroundColor: _getStepColor(3),
                ),
                title: '已收货',
                topTitle: true,
              ),
              EasyStep(
                customStep: CircleAvatar(
                  radius: activeStep == 4 ? 12.0 : 8.0,
                  backgroundColor: _getStepColor(4),
                ),
                title: '交易完成',
              ),
              EasyStep(
                customStep: CircleAvatar(
                  backgroundColor: _getStepColor(5),
                ),
                title: '已评价',
                topTitle: true,
              ),
            ],
            onStepReached: (index) => setState(() => activeStep = index),
          ),
        ),
      ),
    );
  }

  Color? _getStepColor(index) {
    if (activeStep == index) return activeColor;
    if (activeStep > index) return finishedColor;
    return toBeFinishedColor;
  }

  bool _allowTabStepping(int index, StepEnabling enabling) {
    return enabling == StepEnabling.sequential
        ? index <= reachedStep
        : reachedSteps.contains(index);
  }

  /// Returns the next button.
  Widget _nextStep(StepEnabling enabling) {
    return IconButton(
      onPressed: () {
        if (activeStep2 < upperBound) {
          setState(() {
            if (enabling == StepEnabling.sequential) {
              ++activeStep2;
              if (reachedStep < activeStep2) {
                reachedStep = activeStep2;
              }
            } else {
              activeStep2 =
                  reachedSteps.firstWhere((element) => element > activeStep2);
            }
          });
        }
      },
      icon: const Icon(Icons.arrow_forward_ios),
    );
  }

  /// Returns the previous button.
  Widget _previousStep(StepEnabling enabling) {
    return IconButton(
      onPressed: () {
        if (activeStep2 > 0) {
          setState(() => enabling == StepEnabling.sequential
              ? --activeStep2
              : activeStep2 =
                  reachedSteps.lastWhere((element) => element < activeStep2));
        }
      },
      icon: const Icon(Icons.arrow_back_ios),
    );
  }
}

enum StepEnabling { sequential, individual }
