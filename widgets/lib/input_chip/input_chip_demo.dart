import 'package:flutter/material.dart';

class InputChipDemo extends StatefulWidget {
  const InputChipDemo({Key? key}) : super(key: key);

  @override
  State<InputChipDemo> createState() => _InputChipDemoState();
}

class _InputChipDemoState extends State<InputChipDemo> {
  final _techList = [
    TechItemModel(
      name: 'Flutter',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: 'Android',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: '前端',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: 'JavaScript',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: '后端',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: '大数据',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: 'iOS',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: 'Python',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: '人工智能',
      selected: false,
      color: Colors.grey[100]!,
    ),
    TechItemModel(
      name: 'Golang',
      selected: false,
      color: Colors.grey[100]!,
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
          title: const Text('Input Chip'), backgroundColor: Colors.red[400]!),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(20.0),
              child: const Text(
                '选择你的技术栈',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  height: 2,
                ),
              ),
            ),
            Wrap(
              spacing: 10.0,
              children: _techList
                  .map(
                    (item) => InputChip(
                      labelPadding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      backgroundColor: item.color,
                      selectedColor: Colors.red[400],
                      selected: item.selected,
                      onSelected: (isSelected) {
                        setState(() {
                          item.selected = isSelected;
                        });
                      },
                      avatar: item.selected
                          ? null
                          : CircleAvatar(
                              backgroundColor: Colors.lightBlue,
                              child: Text(
                                item.name.substring(0, 1),
                                style: const TextStyle(color: Colors.white),
                              ),
                            ),
                      label: Text(
                        item.name,
                      ),
                    ),
                  )
                  .toList(),
            ),
          ],
        ),
      ),
    );
  }
}

class TechItemModel {
  final String name;
  bool selected;
  final Color color;

  TechItemModel(
      {required this.name, required this.selected, required this.color});
}
