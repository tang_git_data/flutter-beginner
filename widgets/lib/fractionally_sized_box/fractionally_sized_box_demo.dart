import 'package:flutter/material.dart';

class FractionallySizedBoxDemo extends StatefulWidget {
  const FractionallySizedBoxDemo({Key? key}) : super(key: key);

  @override
  State<FractionallySizedBoxDemo> createState() =>
      _FractionallySizedBoxDemoState();
}

class _FractionallySizedBoxDemoState extends State<FractionallySizedBoxDemo> {
  final _widthFactor = 0.85;
  final _heightFactor = 0.6;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        backgroundColor: Colors.grey[50],
        appBar: AppBar(
            title: const Text('FractionallySizedBox'),
            backgroundColor: Colors.red[400]!),
        body: Center(
          child: FractionallySizedBox(
            widthFactor: 0.5,
            heightFactor: 0.5,
            child: Container(
              color: Colors.red[200],
              child: FractionallySizedBox(
                widthFactor: 0.5,
                heightFactor: 0.5,
                child: Container(
                  color: Colors.red[400],
                  child: FractionallySizedBox(
                    widthFactor: 0.5,
                    heightFactor: 0.5,
                    child: Container(
                      color: Colors.red[600],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        bottomSheet: Container(
          height: 68,
          decoration: BoxDecoration(
              color: Colors.black87,
              border: Border(
                top: BorderSide(
                    width: 1.0, color: Colors.grey[300]!.withAlpha(20)),
              )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Flexible(
                flex: 1,
                child: FractionallySizedBox(
                  widthFactor: _widthFactor,
                  heightFactor: _heightFactor,
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(
                        Icons.shopify,
                        color: Colors.red,
                      ),
                      Icon(
                        Icons.favorite_outline,
                        color: Colors.red,
                      ),
                      Icon(
                        Icons.star_border,
                        color: Colors.red,
                      )
                    ],
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: FractionallySizedBox(
                  widthFactor: _widthFactor,
                  heightFactor: _heightFactor,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.yellow[700],
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                    ),
                    child: TextButton(
                      onPressed: () {},
                      child: const Text(
                        '加入购物车',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: FractionallySizedBox(
                  widthFactor: _widthFactor,
                  heightFactor: _heightFactor,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.red[600],
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                    ),
                    child: TextButton(
                      onPressed: () {},
                      child: const Text(
                        '立即购买',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
