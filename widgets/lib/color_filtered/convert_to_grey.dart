import 'package:flutter/material.dart';

class ConvertToGreyDemo extends StatefulWidget {
  const ConvertToGreyDemo({Key? key}) : super(key: key);

  @override
  State<ConvertToGreyDemo> createState() => _ConvertToGreyDemoState();
}

class _ConvertToGreyDemoState extends State<ConvertToGreyDemo> {
  var _showGrey = false;
  final greyScale = const ColorFilter.matrix(<double>[
    0.2126,
    0.7152,
    0.0722,
    0,
    0,
    0.2126,
    0.7152,
    0.0722,
    0,
    0,
    0.2126,
    0.7152,
    0.0722,
    0,
    0,
    0,
    0,
    0,
    1,
    0
  ]);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ColorFiltered'),
        backgroundColor: _showGrey ? Colors.grey : Colors.red[400]!,
      ),
      body: ListView.separated(
        itemBuilder: (_, index) {
          if (_showGrey) {
            return ColorFiltered(
              colorFilter: greyScale,
              child: const ImageTextItem(),
            );
          } else {
            return const ImageTextItem();
          }
        },
        separatorBuilder: (_, index) {
          return const SizedBox(height: 10.0);
        },
        itemCount: 20,
      ),
      floatingActionButton: IconButton(
          onPressed: () {
            setState(() {
              _showGrey = !_showGrey;
            });
          },
          color: _showGrey ? Colors.grey : Colors.red[400]!,
          icon: const Icon(Icons.change_circle)),
    );
  }
}

class ImageTextItem extends StatefulWidget {
  const ImageTextItem({Key? key}) : super(key: key);

  @override
  State<ImageTextItem> createState() => _ImageTextItemState();
}

class _ImageTextItemState extends State<ImageTextItem> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset('images/girl.jpeg', width: 80.0),
        const SizedBox(width: 8.0),
        const Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '这是标题',
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.red,
                ),
                textAlign: TextAlign.left,
              ),
              SizedBox(height: 8.0),
              Text(
                '这是说明文字',
                style: TextStyle(fontSize: 14.0, color: Colors.grey),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
