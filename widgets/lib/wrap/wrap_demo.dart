import 'package:flutter/material.dart';

class WrapDemo extends StatefulWidget {
  const WrapDemo({Key? key}) : super(key: key);

  @override
  State<WrapDemo> createState() => _WrapDemoState();
}

class _WrapDemoState extends State<WrapDemo> {
  final _tagsLists = ['假一赔十', '满300减50', '运费险', '线下同款', '7天无理由退换', '官方正品'];

  final _imagesLists = [
    'images/earth.jpeg',
    'images/fire.png',
    'images/girl.jpeg',
    'images/island-coder.png',
    'images/logo.png',
    'images/mb.jpeg'
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      appBar:
          AppBar(title: const Text('Wrap'), backgroundColor: Colors.red[400]!),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Wrap(
              direction: Axis.horizontal,
              spacing: 8.0,
              alignment: WrapAlignment.start,
              verticalDirection: VerticalDirection.down,
              children: List.generate(
                _tagsLists.length,
                (index) => Chip(
                  label: Text(_tagsLists[index]),
                  backgroundColor: Colors.grey[300],
                  labelStyle: const TextStyle(color: Colors.black87),
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            Wrap(
              spacing: 8.0,
              runSpacing: 8.0,
              children: List.generate(
                _imagesLists.length,
                (index) => Container(
                  width: 120,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                      image: AssetImage(_imagesLists[index]),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
