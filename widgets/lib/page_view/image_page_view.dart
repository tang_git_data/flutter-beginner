import 'package:flutter/material.dart';

class ImagePageView extends StatelessWidget {
  final String imageName;
  const ImagePageView({Key? key, required this.imageName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Image.asset(
        imageName,
        fit: BoxFit.fitHeight,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
      ),
    );
  }
}
