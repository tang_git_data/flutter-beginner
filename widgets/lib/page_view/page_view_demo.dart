import 'package:flutter/material.dart';
import 'package:widgets/page_view/image_page_view.dart';

class PageViewDemo extends StatefulWidget {
  const PageViewDemo({Key? key}) : super(key: key);

  @override
  State<PageViewDemo> createState() => _PageViewDemoState();
}

class _PageViewDemoState extends State<PageViewDemo> {
  late PageController _pageController;
  int _pageIndex = 1;

  @override
  void initState() {
    _pageController = PageController(
      initialPage: _pageIndex,
      viewportFraction: 1.0,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: PageView(
        scrollDirection: Axis.vertical,
        onPageChanged: (index) {
          _pageIndex = index;
        },
        controller: _pageController,
        allowImplicitScrolling: false,
        padEnds: false,
        reverse: false,
        children: const [
          ImagePageView(imageName: 'images/earth.jpeg'),
          ImagePageView(imageName: 'images/island-coder.png'),
          ImagePageView(imageName: 'images/mb.jpeg'),
          ImagePageView(imageName: 'images/earth.jpeg'),
          ImagePageView(imageName: 'images/island-coder.png'),
          ImagePageView(imageName: 'images/mb.jpeg'),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _pageController.animateToPage(
            0,
            duration: const Duration(
              milliseconds: 1000,
            ),
            curve: Curves.easeOut,
          );
        },
        backgroundColor: Colors.black.withAlpha(180),
        child: const Icon(
          Icons.arrow_upward,
          color: Colors.white,
        ),
      ),
    );
  }
}
