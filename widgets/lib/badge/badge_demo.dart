import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;

class BadgeDemo extends StatefulWidget {
  const BadgeDemo({Key? key}) : super(key: key);

  @override
  State<BadgeDemo> createState() => _BadgeDemoState();
}

class _BadgeDemoState extends State<BadgeDemo> {
  int _badgeNumber = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Badge Demo'),
        actions: [
          badges.Badge(
            showBadge: _badgeNumber > 0,
            badgeContent: Text(
              _badgeNumber < 99 ? _badgeNumber.toString() : '99+',
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 11.0,
              ),
            ),
            position: badges.BadgePosition.topEnd(top: 4, end: 4),
            badgeStyle: const badges.BadgeStyle(
              padding: EdgeInsets.all(4),
            ),
            child: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.message_outlined,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: Center(
        child: badges.Badge(
          showBadge: _badgeNumber > 0,
          badgeContent: Text(
            _badgeNumber < 99 ? _badgeNumber.toString() : '99+',
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 10.0,
            ),
          ),
          position: badges.BadgePosition.topStart(top: -10, start: -10),
          badgeStyle: badges.BadgeStyle(
            shape: badges.BadgeShape.square,
            badgeColor: Colors.blue,
            padding: const EdgeInsets.all(6),
            borderRadius: BorderRadius.circular(4),
            borderSide: const BorderSide(color: Colors.white, width: 2),
            borderGradient: const badges.BadgeGradient.linear(
                colors: [Colors.red, Colors.black]),
            badgeGradient: const badges.BadgeGradient.linear(
              colors: [Colors.blue, Colors.yellow],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
            elevation: 0,
          ),
          child: Image.asset(
            'images/girl.jpeg',
            width: 200,
            height: 200,
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(
          icon: badges.Badge(
              showBadge: _badgeNumber > 0,
              badgeContent: Text(
                _badgeNumber < 99 ? _badgeNumber.toString() : '99+',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 11.0,
                ),
              ),
              badgeAnimation: const badges.BadgeAnimation.fade(),
              position: badges.BadgePosition.topEnd(top: -4, end: -6),
              badgeStyle: const badges.BadgeStyle(
                padding: EdgeInsets.all(2),
              ),
              child: const Icon(Icons.home_outlined)),
          label: '首页',
        ),
        const BottomNavigationBarItem(
          icon: Icon(
            Icons.star_border,
          ),
          label: '推荐',
        ),
        const BottomNavigationBarItem(
          icon: Icon(
            Icons.account_circle_outlined,
          ),
          label: '我的',
        ),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _badgeNumber += 1;
          });
        },
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}
