import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class SpeedDialDemo extends StatefulWidget {
  const SpeedDialDemo({Key? key}) : super(key: key);

  @override
  State<SpeedDialDemo> createState() => _SpeedDialDemoState();
}

class _SpeedDialDemoState extends State<SpeedDialDemo> {
  var _speedDialDirection = SpeedDialDirection.left;
  var _childButtonSize = 56.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Speed Dial Demo'),
      ),
      body: ListView(children: [
        TextButton(
          onPressed: () {
            setState(() {
              _speedDialDirection = SpeedDialDirection.left;
              _childButtonSize = 56.0;
            });
          },
          child: const Text('横向展开'),
        ),
        TextButton(
          onPressed: () {
            setState(() {
              _speedDialDirection = SpeedDialDirection.up;
              _childButtonSize = 44.0;
            });
          },
          child: const Text('纵向展开'),
        ),
      ]),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey[600],
        selectedLabelStyle: const TextStyle(
          color: Colors.blue,
        ),
        unselectedLabelStyle: TextStyle(
          color: Colors.grey[600],
        ),
        showUnselectedLabels: true,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home_outlined,
            ),
            label: '首页',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.star_border,
            ),
            label: '推荐',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.map_outlined,
            ),
            label: '发现',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_circle_outlined,
            ),
            label: '我的',
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: SpeedDial(
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        icon: Icons.rocket,
        elevation: 4.0,
        buttonSize: const Size(44, 44),
        childrenButtonSize: Size(_childButtonSize, _childButtonSize),
        animationAngle: -pi / 4, // 图标的旋转角度，和图标本身的朝向没关系
        activeIcon: Icons.rocket_launch,
        direction: _speedDialDirection,
        spaceBetweenChildren: 4.0,
        spacing: 4.0,
        children: [
          SpeedDialChild(
            child: const Icon(Icons.add),
            backgroundColor: Colors.green,
            foregroundColor: Colors.white,
            onTap: () {},
          ),
          SpeedDialChild(
            child: const Icon(Icons.settings),
            backgroundColor: Colors.orange[300],
            foregroundColor: Colors.white,
            onTap: () {},
          ),
          SpeedDialChild(
            child: const Icon(Icons.person),
            backgroundColor: Colors.purple[300],
            foregroundColor: Colors.white,
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
