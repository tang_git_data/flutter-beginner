import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:widgets/password_validator/password_validator.dart';

import 'autocomplete/auto_complete_demo.dart';
import 'easy_stepper/easy_stepper_demo.dart';

void main() {
  runApp(const MyApp());
  LicenseRegistry.addLicense(() async* {
    yield const LicenseEntryWithLineBreaks(
      ['关于岛上码农'],
      '我是岛上码农，微信公众号同名。如有问题可以加本人微信交流，微信号：island-coder。',
    );
  });

  LicenseRegistry.addLicense(() async* {
    yield const LicenseEntryWithLineBreaks(
      ['关于岛上码农'],
      '使用时请注明来自岛上码农、。',
    );
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const EasyStepperDemo(),
      localizationsDelegates: const [MyMaterialLocalizationsDelegate()],
    );
  }
}

class MyMaterialLocalizationsDelegate
    extends LocalizationsDelegate<MaterialLocalizations> {
  const MyMaterialLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<MaterialLocalizations> load(Locale locale) async {
    // 在这里加载你的自定义本地化资源
    final myTranslations = MyMaterialLocalizations(); // 自定义的本地化资源类
    return Future.value(myTranslations);
  }

  @override
  bool shouldReload(
          covariant LocalizationsDelegate<MaterialLocalizations> old) =>
      false;
}

class MyMaterialLocalizations extends DefaultMaterialLocalizations {
  @override
  String get viewLicensesButtonLabel => '查看版权信息'; // 自定义的文本

  @override
  String get closeButtonLabel => '关闭';

  // 可以重写其他需要自定义的文本
}
