import 'package:flutter/material.dart';
import 'package:home_framework/models/goods_entity.dart';

class CartModel extends ChangeNotifier {
  List<GoodsEntity> _items = [];

  double get totalPrice {
    double totalPrice = 0.0;
    _items.forEach((goods) {
      totalPrice += goods.price;
    });

    return totalPrice;
  }

  List<GoodsEntity> get items => _items;

  void add(GoodsEntity goods) {
    if (!isInCart(goods)) {
      _items.add(goods);

      notifyListeners();
    }
  }

  bool isInCart(GoodsEntity goods) {
    return _items.any((element) => element.id == goods.id);
  }

  void removeAll() {
    if (_items.length > 0) {
      _items.clear();
      notifyListeners();
    }
  }
}
