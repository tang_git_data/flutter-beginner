import 'package:flutter/material.dart';
import 'package:home_framework/states/face_emotion.dart';
import 'package:home_framework/states/model_binding_v2.dart';

class Xiaofu3 extends StatelessWidget {
  Xiaofu3({Key key}) : super(key: key) {
    print('ModelBinding=====constructor: 小芙');
  }

  @override
  Widget build(BuildContext context) {
    print('ModelBinding=====build：小芙');
    return Center(
      child: Column(children: [
        Text('小芙的表情：${ModelBindingV2.of<FaceEmotion>(context).emotion}'),
        TextButton(
            onPressed: () {
              ModelBindingV2.update<FaceEmotion>(
                  context, FaceEmotion(emotion: '高兴'));
            },
            child: Text('小芙表情变了')),
      ]),
    );
  }
}
