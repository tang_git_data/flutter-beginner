import 'package:floor/floor.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'key_value/key_value_demo.dart';
import 'floor/memo_list.dart';
import 'floor/memo.dart';

GetIt getIt = GetIt.instance;

final migration1to2 = Migration(1, 2, (database) async {
  await database.execute('ALTER TABLE Memo ADD COLUMN category TEXT');
  await database.update('Memo', {'category': ''});
});

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final database = await $FloorMemoDatabase
      .databaseBuilder('app_database.db')
      .addMigrations([migration1to2]).build();

  final dao = database.memoDao;

  getIt.registerSingleton<MemoDao>(dao, signalsReady: true);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.orange[50],
        brightness: Brightness.dark,
      ),
      home: const MemoListScreen(),
    );
  }
}
