import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'memo_add.dart';
import 'memo.dart';
import 'memo_edit.dart';

class MemoListScreen extends StatefulWidget {
  const MemoListScreen({Key? key}) : super(key: key);

  @override
  MemoListScreenState createState() => MemoListScreenState();
}

class MemoListScreenState extends State<MemoListScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Memo> _memoList = [];

  @override
  void initState() {
    super.initState();
    _refreshMemoList();
  }

  void _refreshMemoList({String? searchKey}) async {
    List<Memo> memoList = searchKey == null
        ? await GetIt.I<MemoDao>().findAllMemos()
        : await GetIt.I<MemoDao>().findMemoWithSearchKey('%$searchKey%');
    setState(() {
      _memoList = memoList;
    });
  }

  void _deleteMemo(Memo memo) async {
    final confirmed = await _showDeleteConfirmationDialog(memo);
    if (confirmed != null && confirmed) {
      await GetIt.I<MemoDao>().deleteMemo(memo);
      _refreshMemoList();
      if (!mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('已删除 "${memo.title}"'),
        duration: const Duration(seconds: 2),
      ));
    }
  }

  Future<bool?> _showDeleteConfirmationDialog(Memo memo) async {
    return showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('删除备忘录'),
          content: Text('确定要删除 "${memo.title}"这条备忘录吗？'),
          actions: [
            TextButton(
              child: const Text(
                '取消',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () => Navigator.pop(context, false),
            ),
            TextButton(
              child: Text('删除',
                  style: TextStyle(
                    color: Colors.red[300],
                  )),
              onPressed: () => Navigator.pop(context, true),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('备忘录'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              decoration: InputDecoration(
                hintText: '搜索备忘录',
                prefixIcon: const Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
              ),
              onChanged: (value) {
                _refreshMemoList(searchKey: value);
              },
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _memoList.length,
              itemBuilder: (context, index) {
                Memo memo = _memoList[index];
                return ListTile(
                  title: Text(memo.title),
                  subtitle: Row(
                    children: [
                      memo.category != null
                          ? InputChip(
                              label: Text(
                                memo.category!,
                                style: const TextStyle(color: Colors.black),
                              ),
                              elevation: 6.0,
                              labelPadding:
                                  const EdgeInsets.fromLTRB(8, -2, 8, -2),
                              isEnabled: false,
                              showCheckmark: false,
                              selected: true,
                              selectedColor: Colors.blue[50],
                            )
                          : Container(),
                      SizedBox(
                        width: memo.category != null ? 8.0 : 0.0,
                      ),
                      Expanded(
                        child: Text(
                          '${DateFormat.yMMMd().format(memo.modifiedTime)}更新',
                        ),
                      ),
                    ],
                  ),
                  minVerticalPadding: 8.0,
                  onTap: () {
                    _navigateToEditScreen(memo);
                  },
                  trailing: IconButton(
                    icon: const Icon(Icons.delete_forever_outlined),
                    onPressed: () {
                      _deleteMemo(memo);
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        child: const Icon(Icons.add),
        onPressed: () async {
          _navigateToAddScreen();
        },
      ),
    );
  }

  _navigateToAddScreen() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const MemoAddScreen()),
    );
    if (result != null) {
      _refreshMemoList();
    }
  }

  _navigateToEditScreen(Memo memo) async {
    final count = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MemoEditScreen(memo: memo)),
    );
    if (count != null && count > 0) {
      _refreshMemoList();
    }
  }
}
