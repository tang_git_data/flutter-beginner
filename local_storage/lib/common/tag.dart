import 'package:flutter/material.dart';

class Tag extends StatelessWidget {
  final String text;
  final VoidCallback onDeleted;

  const Tag({Key? key, required this.text, required this.onDeleted})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InputChip(
      label: Text(text),
      onDeleted: onDeleted,
    );
  }
}
