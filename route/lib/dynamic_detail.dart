import 'package:flutter/material.dart';

class DynamicDetail extends StatelessWidget {
  const DynamicDetail({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> routeParams =
        ModalRoute.of(context).settings?.arguments;

    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text('动态详情'),
          brightness: Brightness.dark,
        ),
        body: Center(
          child: Text(
              "产品 id: ${routeParams['id']}, event: ${routeParams['event']} "),
        ),
      ),
      onWillPop: () async {
        Navigator.of(context).pop({'id': routeParams['id']});
        return true;
      },
    );
  }
}

class DynamicDetailPage extends StatelessWidget {
  //const DynamicDetail({Key key}) : super(key: key);
  final String id;
  const DynamicDetailPage(this.id, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text('动态详情'),
          brightness: Brightness.dark,
        ),
        body: Center(
          child: Text("产品 id: ${this.id}"),
        ),
      ),
      onWillPop: () async {
        Navigator.of(context).pop({'id': this.id});
        return true;
      },
    );
  }
}
